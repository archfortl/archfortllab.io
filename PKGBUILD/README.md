## Packages
- `libf2c`
- `gcc34-bin`
- `gcc34-c++-bin`
- `gcc34-g77-bin`
- `f2c`

# Fortran 77 Compilers
## 1. GCC 3.4.6 - C, C++ & Fortran
The last GCC version that contains the Fortran 77 compiler `g77` or `f77`.
Install the `gcc34-g77-bin` package.

## 2. F2C
Converts Fortran 77 to code to C code and provides a wrapper for `f77` compiler.
Install the `f2c` package.
NB - F2C can be installed with libf2c as a shared library (libf2c.so) or with it as a normal one (libf2c.a). Please comment/ uncomment the relevant sections to choose between the two. An extra "-lm" flag is needed while compiling C files generated the shared library variant.
